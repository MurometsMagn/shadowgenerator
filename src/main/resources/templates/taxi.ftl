<!DOCTYPE html>
<html>
<head>
    <title>CSS Taxi Animation Effects</title>
    <link rel="stylesheet" href="/css/taxiStyle.css">
</head>
<body>
    <div class="container">
        <div class="road">
            <div class="taxi">
                <div class="light-beam"></div>
                <span>
                    <b></b>
                    <i></i>
                </span>
            </div>
            <div class="taxi">
                <div class="light-beam"></div>
                <span>
                    <b></b>
                    <i></i>
                </span>
            </div>
        </div>
    </div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Glass Shutter Effects</title>
    <link rel="stylesheet" href="/css/lesson_9_glassShutterStyle.css">
</head>
<body>
    <section>
        <div>
            <h2 id="rangeValue">0</h2>
            <div id="fillRangeTopLeft" class="glass"></div>
            <div id="fillRangeTopRight" class="glass"></div>
            <div id="fillRangeBottomLeft" class="glass"></div>
            <div id="fillRangeBottomRight" class="glass"></div>
            <input type="range" class="range" value="0" min="0" max="100"
            onmousemove="rangeSlider(this.value)"
            onchange="rangeSlider(this.value)">
        </div>
    </section>

    <script>
        function rangeSlider(value){
            document.getElementById('rangeValue').innerHTML = value;

            document.getElementById('fillRangeTopLeft').style.width = +value/2+'%';

            document.getElementById('fillRangeTopRight').style.height = +value/2+'%';

            document.getElementById('fillRangeBottomLeft').style.height = +value/2+'%';

            document.getElementById('fillRangeBottomRight').style.width = +value/2+'%';
        }
    </script>
</body>
</html>
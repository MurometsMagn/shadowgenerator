package com.example.ShadowGenerator.tryPack;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class CurrencyCache {
    private Map<CurrencyPair, BigDecimal> cache = new HashMap<>();

    private void testMethod() {
        Properties properties = new Properties();
        properties.getX();
        properties.setX(8);
    }

    @Data
    @AllArgsConstructor
    public static class CurrencyPair {
        private long timestamp;
        private String sourceCurrency;
        private String targetCurrency;
    }

}

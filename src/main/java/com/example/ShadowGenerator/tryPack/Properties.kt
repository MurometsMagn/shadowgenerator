package com.example.ShadowGenerator.tryPack

class Properties {
    var x: Int
        get() {
            return 0
        }
        set(value) {
            println(value)
        }

    fun testMth() {
        x = 10     //напечатает 10
        println(x) // напечатает 0
    }

    companion object{
        @JvmStatic
        fun main(args: Array<String>) {
            val p = Properties()
            p.testMth()
        }
    }
}



/*
   В Котлин прогеру доступны только методы и свойства, полями управляет только сам Котлин.
   Т.к. св-ва -это по сути один или два метода (гетер-сетер), то для них уровень доступа по умолчанию public -
   вполне оправдан.
 */
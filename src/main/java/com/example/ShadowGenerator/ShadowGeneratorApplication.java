package com.example.ShadowGenerator;

//by lesson: https://youtu.be/NCYdVU-43IU

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShadowGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShadowGeneratorApplication.class, args);
	}

}

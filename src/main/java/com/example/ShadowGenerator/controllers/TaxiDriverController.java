package com.example.ShadowGenerator.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TaxiDriverController {

    //by lesson https://youtu.be/WJqd8Sof7lE
    @GetMapping("/taxi")
    public String taxi() {
        return "taxi";
    }

    //by lesson https://youtu.be/_jOqYe0eFqY
    @GetMapping("/tea_cup")
    public String teaCup() {
        return "tea_cup";
    }

    //https://youtu.be/1JHCv8Z1sy4
    @GetMapping("/glass_shutter")
    public String glassShutter() {
        return "lesson_9_glass_shutter";
    }

    //https://youtu.be/2KsYS1qQ9qQ
    @GetMapping("/ghost_text")
    public String ghostText() {
        return "ghost_text";
    }

    //https://youtu.be/e-LPHHRRuoM
    /*  https://cdnjs.com/libraries/font-awesome/4.7.0
    https://fontawesome.com/v4/icons/
    https://micku7zu.github.io/vanilla-tilt.js/ */
    @GetMapping("/social_media")
    public String socialMedia() {
        return "social_media";
    }

    // https://youtu.be/z3TgmTi42ic
    @GetMapping("/glowing_checkbox")
    public String glowingCheckbox() {
        return "glowing_checkbox";
    }
}
